from azureml.core import Workspace
from azureml.core.webservice import AciWebservice, Webservice
from azureml.core.model import Model, InferenceConfig

def azure_register(workspace_name: str, subscription_id: str, resource_group: str):
    ws = Workspace.get(name=workspace_name,
                      subscription_id=subscription_id,
                      resource_group=resource_group
                     )
    
    Model.register(
        model_name=input("Enter Model Name: \n"),
        model_path='./DirectoryToDeploy',
        description='',
        workspace=ws
    )


if __name__ == "__main__":
    azure_register('ml_workspace', '3eb1d5cf-ed74-4504-8cac-8384c0149ae6', 'ml_resource_group')
