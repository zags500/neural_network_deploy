import random
import os
import requests
import json

# URL for the web service
#scoring_uri = 'http://36e31985-5fd8-4bc5-8dd3-c14b44a8eca3.eastus2.azurecontainer.io/score'
scoring_uri = 'http://localhost:8890/score'
# If the service is authenticated, set the key or token
key = '<your key or token>'

# Two sets of data to score, so we get two results back
#data = "{\"data\":" + json.dumps([[[str((random.random()-0.5)*2*600) for i in range(60)] for j in range(15)] for i in range(240)]) + "}"
#data = "{\"data\":" + json.dumps([[[0 for i in range(60)] for j in range(15)] for i in range(240)]) + "}"
with open(os.path.join(os.path.dirname(__file__), 'text.txt'), 'r') as file:
    data = "{\"data\":" + file.read() + "}"

print()
print(data)
print()

# Set the content type
headers = {'Content-Type': 'application/json'}

# Make the request and display the response
resp = requests.post(scoring_uri, data, headers=headers)
print()
print(resp.json())
