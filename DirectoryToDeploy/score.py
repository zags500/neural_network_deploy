import json
import numpy as np
import os
import tensorflow as tf
import logging

# only needed for testing
import random

tf.compat.v1.disable_v2_behavior()
tf.compat.v1.enable_resource_variables()

from azureml.core.model import Model

def init():
    """This is called after deployment."""
    load_from_checkpoint()
    

def run(raw_data: str) -> str:
    """This is called after a request is recieved. So that this code will be the same regardless of the
    neural network used, the data should have already have been processed into the correct format."""
    try:
        result = get_inference(json.loads(raw_data)['data'])
        return json.dumps({"result": result})
    except Exception as e:
        result = str(repr(e))
        return json.dumps({"error": result})
    

def load_from_checkpoint():
    tf_model_folder = get_model_folder()
    
    global sess
    sess = tf.compat.v1.Session()

    # -- Begin network specific stuff --
    
    global output, x, y
        
    rnn_size=128
    n_classes = 2  # MALE AND FEMALE
    no_of_data_points = 60 # 60 for 2D
    
    weights = tf.compat.v1.get_variable("weights", shape=[rnn_size, n_classes], initializer = tf.zeros_initializer)
    biases = tf.compat.v1.get_variable("biases", shape=[n_classes], initializer = tf.zeros_initializer)

    x = tf.compat.v1.placeholder('float', [None, None, no_of_data_points])
    y = tf.compat.v1.placeholder('float', [None, n_classes])

    data = tf.compat.v1.transpose(x, [1,0,2])

    lstm_cell = tf.compat.v1.nn.rnn_cell.BasicLSTMCell(rnn_size)
    outputs, states = tf.compat.v1.nn.dynamic_rnn(lstm_cell, data, dtype=tf.compat.v1.float32, time_major=True)
    output = tf.compat.v1.add(tf.compat.v1.matmul(outputs[-1], weights), biases, name='prediction')

    # -- End network specific stuff --
    
    loader = tf.compat.v1.train.Saver()
    loader.restore(sess, tf.compat.v1.train.latest_checkpoint(tf_model_folder))


def get_model_folder() -> str:
    root_folder = os.getenv('AZUREML_MODEL_DIR')

    # If this is running as a script
    if root_folder == None:
        root_folder = "./"

    else:
        root_folder = os.path.join(root_folder, "DirectoryToDeploy")
    
    tf_model_folder = os.path.join(root_folder, 'Put_Checkpoint_Files_In_Here')
    logging.warning(os.path.abspath(tf_model_folder))
    return tf_model_folder
    

def get_inference(data : list):
    with sess.as_default():
        return output.eval({x:data}).tolist()


if __name__ == "__main__":
    init()
    print(json.loads(run("{\"data\":[[[\"1\",\"1\",\"0\",\"0\",\"0\",\"1\",\"0\",\"0\",\"0\",\"0\",\"1\",\"0\",\"0\",\"0\",\"0\",\"1\",\"0\",\"0\",\"0\",\"0\",\"1\",\"0\",\"0\",\"0\",\"0\",\"1\",\"0\",\"0\",\"0\",\"0\",\"1\",\"0\",\"0\",\"0\",\"0\",\"1\",\"0\",\"0\",\"0\",\"0\",\"1\",\"0\",\"0\",\"0\",\"0\",\"1\",\"0\",\"0\",\"0\",\"0\",\"1\",\"0\",\"0\",\"0\",\"0\",\"1\",\"0\",\"0\",\"0\",\"0\"]]]}")))
    print(json.loads(run("{\"data\":[[\"1\",\"0\"]]}")))
    print(json.loads(run("{\"data\":[[\"0\",\"1\"]]}")))
    print(json.loads(run("{\"data\":[[\"0\",\"0\"]]}")))
    print(json.loads(run("{\"data\":[[\"1\",\"1\"]]}")))
    print(json.loads(run("{\"data\":[[\"0.9999\",\"1\"]]}")))
    print(json.loads(run("{\"data\":[[\"0.5\",\"0.25\"]]}")))
    print(json.loads(run("{\"data\":[[\"apple\",\"orange\"]]}")))
    print(json.loads(run("{\"data\":" + json.dumps([[[str((random.random()-0.5)*2*600) for i in range(60)] for j in range(15)] for i in range(1000)]) + "}")))
