# https://docs.microsoft.com/en-us/azure/machine-learning/how-to-deploy-azure-container-instance
# https://docs.microsoft.com/en-us/azure/machine-learning/how-to-deploy-and-where
# https://docs.microsoft.com/en-us/python/api/overview/azure/ml/?view=azure-ml-py

from azureml.core import Workspace
from azureml.core.webservice import AciWebservice, Webservice, LocalWebservice
from azureml.core.model import Model, InferenceConfig
from azureml.core.environment import Environment
from azureml.core.authentication import InteractiveLoginAuthentication

#from azureml.core.webservice import LocalWebservice

def azure_deploy(workspace_name: str, subscription_id: str, resource_group: str):
    InteractiveLoginAuthentication()
    ws = Workspace.get(name=workspace_name,
                      subscription_id=subscription_id,
                      resource_group=resource_group
                      )

    myenv = Environment.from_conda_specification(name="env", file_path="./DirectoryToDeploy/myenv.yml")
    inference_config = InferenceConfig(entry_script="./DirectoryToDeploy/score.py", environment=myenv)

    index = 0
    
    
    while True:
        print("The following models have been found in your workspace:")
        for i in range(len(Model.list(ws))):
            print(str(i) + ") " + Model.list(ws)[i].id)
        print()
        print("Enter the model you wish to pick:")
        try:
            entry = input()
            print()
            index = int(entry)
            if index < len(Model.list(ws)):
                break
        except ValueError:
            pass
        
    model = Model.list(ws)[index]

    print("Attempting to deploy with model " + model.id + ".")
    print("If you encounter an error whilst deploying locally, it is most likely you have forgot to turn docker on.")
    
    #deployment_config = AciWebservice.deploy_configuration(cpu_cores = 0.1, memory_gb = 0.5)
    deployment_config = LocalWebservice.deploy_configuration(port=8890)
    service = Model.deploy(ws, "grpc", [model], inference_config, deployment_config)
    
    try:
        service.wait_for_deployment(show_output = True)
        print(service.state)
    except:
        print(service.state)
        print(service.get_logs())
        raise

if __name__ == "__main__":
    azure_deploy('ml_workspace', '3eb1d5cf-ed74-4504-8cac-8384c0149ae6', 'ml_resource_group')
